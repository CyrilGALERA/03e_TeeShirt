// CONTROLLER ================
function filtreTaille0( tabTshirts, taille) {
    return tabTshirts.filter( t => t.taille === taille);
}

function filtreTaille( tabTshirts, taille) {
    return tabTshirts.filter( function (t) {
        return t.taille === taille;
    });
}

function filtreTaille( tabTshirts, couleur) {
    return tabTshirts.filter( function (t) {
        return t.couleur === couleur;
    });
}