// CONTROLLER ================
function filtreTaille0( tabTshirts, taille) {
    return tabTshirts.filter( t => t.taille === taille);
}
function filtreTaille0( tabTshirts, key, value) {
    return tabTshirts.filter( t => t[key] === value);
}

function filtreTaille( tabTshirts, taille) {
    console.log("filtreTaille");

    return tabTshirts.filter( function (t) {
        return t.taille === taille;
    });
}

function filtreCouleur( tabTshirts, couleur) {
    console.log("filtreCouleur");
    return tabTshirts.filter( function (t) {
        return t.couleur === couleur;
    });
}