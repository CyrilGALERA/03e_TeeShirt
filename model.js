// MODEL ==============================================
//  - class:    TeeShirt
//  - Set:      size
//  - Set:      color
//

class TeeShirt {
    constructor(nom, couleur, taille, image) {
        this.nom = nom;
        this.couleur = couleur;        
        this.taille = taille;
        this.image = image;
    }
}

let teeshirts = [];
teeshirts.push( new TeeShirt( "Stalin",     "Rouge",  "XL", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQB08pf0Ul1HTZYknx-Qlx5qQFHQgMIlkJ-bw&usqp=CAU"));
teeshirts.push( new TeeShirt( "Stalin",     "Blanc",  "L", "https://image.spreadshirtmedia.net/image-server/v1/mp/products/T812A1MPA3811PT17X49Y47D153437341FS2951/views/1,width=550,height=550,appearanceId=1,backgroundColor=F2F2F2,modelId=85,crop=list/drapeau-communiste-rond-use-t-shirt-premium-homme.jpg"));
teeshirts.push( new TeeShirt( "Stalin",     "Blanc", "S",  "https://ih1.redbubble.net/image.1846857347.5673/ssrco,classic_tee,womens,fafafa:ca443f4786,front_alt,square_product,600x600.jpg"));
teeshirts.push( new TeeShirt( "Cap America", "Bleu", "XL",  "https://assets.laboutiqueofficielle.com/w_450,q_auto,f_auto/media/products/2019/05/14/marvel_183703_MEAMERCTS036_NAVY_20190605T110142_01.jpg"));
teeshirts.push( new TeeShirt( "Gaia",       "Vert", "XL",   "https://us.123rf.com/450wm/ozaiachin/ozaiachin1208/ozaiachin120800438/14727815-t-shirt-vert-isol%C3%A9-sur-fond-blanc.jpg?ver=6"));

// console.log(teeshirts);

let size = new Set();
for( let t of teeshirts ) {
    size.add( t.taille );
}

let color = new Set();
for( let t of teeshirts ) {
    color.add( t.couleur );
}

console.log( size );
console.log( color );



